# Orient DB 2.0 Milestone 3 with Tinkerpop 2

## Starting Orient DB on Vagrant 

	sudo /etc/init.d/orientdb.sh start

	#Alternatively 
		/opt/orientdb/bin/server.sh &

	This will start running the server on your system in the background. 
	If you need to kill the server instance, you can search for the pid with ps and then kill the process:

		ps aux | grep server.sh | grep -v grep | awk '{ print $2; }' | xargs kill
		
#When started, the server exposes two separate ports that you can use depending on the protocol you wish to employ:

	2424: This port is opened and used for binary access to the database. 
	This means that you can use clients and drivers that operate with the network binary protocol, 
	or the database console to interact with the database through this port.
	
	2480: This port is opened and used for HTTP interaction through a RESTful API. 
	You can access your server's web tool by visiting this port with your browser.

	Credentials:	uSer : paSS
	   
	Verify that it is running by opening the studio (e.g. http://192.168.50.10:2480/) 
	or run ‘sudo /etc/init.d/orientdb.sh status’.

# Tinkerpo can be reached through
	8182: GremlinServer - Gremlin Server configured with worker thread pool of 1 and boss thread pool of 1
    GremlinServer - Websocket channel started at port 8182.
	   
#Start the console:
	/opt/orientdb/bin/console.sh

#Create a new database:
	create database remote:/yourDatabaseName yourUsername yourPassword local
   
	   
	   
## Tinkerpop REST Usage

[More on the REST API](https://github.com/tinkerpop/rexster/wiki/Basic-REST-API)

Create a node:

    curl -H 'Content-Type: application/json' -X POST 'http://192.168.50.10:8182/graphs/graph/vertices' -d '{"foo":"bar"}'

Search a node by value:

    curl 'http://192.168.50.10:8182/graphs/graph/vertices?key=foo&value=bar'

Create an edge:

    curl -X POST 'http://192.168.50.10:8182/graphs/graph/edges?_outV=4&_inV=8&label=friend'

Gremlin query:

    curl 'http://192.168.50.10:8182/graphs/graph/tp/gremlin?script=g.v(4)'

## License 

Cpt. Pluto